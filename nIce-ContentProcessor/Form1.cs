﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FolderSelect;
using nIceConverter;

namespace nIce_ContentProcessor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowseSource_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (!e.Cancel)
            {
                txtSourceFiles.Text = "";
                foreach (String file in openFileDialog.FileNames)
                {
                    txtSourceFiles.Text += file + Environment.NewLine;
                }
            }
        }

        private void btnBrowseDest_Click(object sender, EventArgs e)
        {
            FolderSelectDialog fsd = new FolderSelectDialog();
            fsd.ShowDialog();
            txtDestFolder.Text = fsd.FileName;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            String[] lines = txtSourceFiles.Text.Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            TextboxWriter writer = new TextboxWriter(txtConsole);
            nIceConverter.nIceConverter converter = new nIceConverter.nIceConverter(writer);
            txtConsole.Text = "";

            if (lines.Length == 0)
            {
                txtConsole.Text = "No files selected.";
            }
            else
            {
                txtConsole.Text = "";
                foreach (String file in lines)
                {
                    writer.WriteLine("*** " + Path.GetFileName(file) + " ***");

                    String dest;
                    if (txtDestFolder.Text != "")
                        dest = txtDestFolder.Text + "\\" + Path.GetFileNameWithoutExtension(file) + ".ncf";
                    else
                        dest = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + ".ncf";

                    converter.Convert(file, dest);

                    writer.WriteLine("");
                }
            }
        }
    }
}
