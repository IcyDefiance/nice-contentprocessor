﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace nIce_ContentProcessor
{
    class TextboxWriter : TextWriter
    {
        TextBox output;

        public TextboxWriter(TextBox output)
        {
            this.output = output;
        }

        public override void WriteLine(string value)
        {
            base.WriteLine(value);
            output.Text += value + Environment.NewLine;
            output.ScrollToCaret();
        }

        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}
