﻿using System.IO;
using System.Collections.Generic;
using OpenTK;

namespace nIceConverter
{
    class Mesh
    {
        public List<MeshElement> Vertices { get; set; }
        public List<uint> Indices { get; set; }

        public Mesh()
        {
            Vertices = new List<MeshElement>();
            Indices = new List<uint>();
        }
    }
}