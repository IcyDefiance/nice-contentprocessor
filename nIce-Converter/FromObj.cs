﻿using System.IO;
using System.Collections.Generic;
using OpenTK;

namespace nIceConverter
{
    static class FromObj
    {
        static public Mesh LoadMesh(string file, TextWriter output)
        {
            Mesh model = new Mesh();
            List<Vector3> vertexLines = new List<Vector3>();
            List<Vector3> normalLines = new List<Vector3>();
            List<Vector2> texcoordLines = new List<Vector2>();
            List<FaceElement> faceLine = new List<FaceElement>();
            bool smooth = false;

            try
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (line.Length >= 7 && line.Substring(0, 7) == "mtllib ")
                        {
                            output.WriteLine("Material not supported yet. Ignored.");
                        }
                        else if (line.Length >= 2 && line.Substring(0, 2) == "o ")
                        {
                            output.WriteLine("Separate objects not supported yet. Ignored.");
                        }
                        else if (line.Length >= 3 && line.Substring(0, 3) == "s 1")
                        {
                            smooth = true;
                        }
                        else if (line.Length >= 2 && line.Substring(0, 2) == "v ")      //position
                        {
                            line = line.Substring(2);
                            string[] parts = line.Split(' ');
                            vertexLines.Add(new Vector3(float.Parse(parts[0]), float.Parse(parts[1]), float.Parse(parts[2])));
                        }
                        else if (line.Length >= 3 && line.Substring(0, 3) == "vt ")     //texcoord
                        {
                            line = line.Substring(3);
                            string[] parts = line.Split(' ');
                            texcoordLines.Add(new Vector2(float.Parse(parts[0]), float.Parse(parts[1])));
                        }
                        else if (line.Length >= 3 && line.Substring(0, 3) == "vn ")
                        {
                            line = line.Substring(3);
                            string[] parts = line.Split(' ');
                            normalLines.Add(new Vector3(float.Parse(parts[0]), float.Parse(parts[1]), float.Parse(parts[2])));
                        }
                        else if (line.Length >= 2 && line.Substring(0, 2) == "f ")      //face
                        {
                            line = line.Substring(2);
                            string[] verts = line.Split(' ');

                            for (int i = 0; i < verts.Length - 2; i++)
                            {
                                for (int j = 0; j < 2; j++)     //create indices and record facelines
                                {
                                    FaceElement faceToAdd = MakeFaceVert(verts[i + j]);
                                    int index = faceLine.FindIndex(x => x == faceToAdd);
                                    if (index == -1)
                                    {
                                        model.Indices.Add((uint)faceLine.Count);
                                        faceLine.Add(faceToAdd);
                                    }
                                    else
                                    {
                                        model.Indices.Add((uint)index);
                                    }
                                }

                                FaceElement lastFace = MakeFaceVert(verts[verts.Length - 1]);
                                int lastIndex = faceLine.FindIndex(x => x == lastFace);
                                if (lastIndex == -1)
                                {
                                    model.Indices.Add((uint)faceLine.Count);
                                    faceLine.Add(lastFace);
                                }
                                else
                                {
                                    model.Indices.Add((uint)lastIndex);
                                }

                            }
                        }
                    }
                }
            }
            catch (System.Exception)
            {
                return null;
            }

            for (int i = 0; i < faceLine.Count; i++)    //make vertices
            {
                MeshElement ele = new MeshElement();

                ele.Position = vertexLines[faceLine[i].PositionIndex];

                if (faceLine[i].TexCoordIndex != -1)
                    ele.TexCoord = texcoordLines[faceLine[i].TexCoordIndex];
                else
                    ele.TexCoord = Vector2.Zero;

                if (faceLine[i].NormalIndex != -1)
                    ele.Normal = normalLines[faceLine[i].NormalIndex];
                else
                    ele.Normal = Vector3.Zero;

                model.Vertices.Add(ele);
            }

            for (int i = 0; i < faceLine.Count; i++)    //make normals if not imported
            {
                if (faceLine[i].NormalIndex == -1)
                {
                    if (smooth)
                    {
                        int dupIndex = faceLine.FindIndex(x => x.PositionIndex == faceLine[i].PositionIndex);
                        while (dupIndex != -1)
                        {
                            int indicesIndex = model.Indices.FindIndex(x => x == (uint)dupIndex);
                            while (indicesIndex != -1)
                            {
                                int triangleStart = indicesIndex / 3 * 3;
                                Vector3 relativePosition1 = model.Vertices[(int)model.Indices[triangleStart + 1]].Position - model.Vertices[(int)model.Indices[triangleStart]].Position;
                                Vector3 relativePosition2 = model.Vertices[(int)model.Indices[triangleStart + 2]].Position - model.Vertices[(int)model.Indices[triangleStart]].Position;
                                model.Vertices[i].Normal += Vector3.Normalize(Vector3.Cross(relativePosition1, relativePosition2));

                                indicesIndex = model.Indices.FindIndex(indicesIndex + 1, x => x == (uint)dupIndex);
                            }

                            dupIndex = faceLine.FindIndex(dupIndex + 1, x => x.PositionIndex == faceLine[i].PositionIndex);
                            if (dupIndex != -1)
                                if (faceLine.Count == 0)
                                    return null;
                        }
                        model.Vertices[i].Normal.Normalize();
                    }
                    else
                    {
                        int triangleStart = model.Indices.FindIndex(x => x == (uint)i) / 3 * 3;
                        Vector3 relativePosition1 = model.Vertices[(int)model.Indices[triangleStart + 1]].Position - model.Vertices[(int)model.Indices[triangleStart]].Position;
                        Vector3 relativePosition2 = model.Vertices[(int)model.Indices[triangleStart + 2]].Position - model.Vertices[(int)model.Indices[triangleStart]].Position;
                        model.Vertices[i].Normal += Vector3.Normalize(Vector3.Cross(relativePosition1, relativePosition2));
                    }
                }
            }

            return model;
        }

        private static FaceElement MakeFaceVert(string vertInfo)
        {
            string[] parts = vertInfo.Split('/');
            FaceElement faceToAdd = new FaceElement();

            faceToAdd.PositionIndex = int.Parse(parts[0]) - 1;

            if (parts.Length >= 2 && parts[1] != "")
                faceToAdd.TexCoordIndex = int.Parse(parts[1]) - 1;
            else
                faceToAdd.TexCoordIndex = -1;

            if (parts.Length == 3)
                faceToAdd.NormalIndex = int.Parse(parts[2]) - 1;
            else
                faceToAdd.NormalIndex = -1;

            return faceToAdd;
        }

        public class FaceElement
        {
            public int PositionIndex;
            public int NormalIndex;
            public int TexCoordIndex;

            public bool Equals(FaceElement other)
            {
                if ((object)other == null) return false;
                return PositionIndex == other.PositionIndex
                    && NormalIndex == other.NormalIndex
                    && TexCoordIndex == other.TexCoordIndex;
            }

            public static bool operator ==(FaceElement lh, FaceElement rh)
            {
                if (System.Object.ReferenceEquals(lh, rh)) return true;
                if (((object)lh == null) || ((object)rh == null)) return false;
                return lh.PositionIndex == rh.PositionIndex
                    && lh.NormalIndex == rh.NormalIndex
                    && lh.TexCoordIndex == rh.TexCoordIndex;
            }

            public static bool operator !=(FaceElement a, FaceElement b)
            {
                return !(a == b);
            }
        }
    }
}
