﻿using OpenTK;

namespace nIceConverter
{
    public class MeshElement
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoord;
    }
}
