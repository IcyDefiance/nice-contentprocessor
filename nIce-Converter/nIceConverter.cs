﻿using System;
using System.IO;
using System.Text;
using OpenTK;

namespace nIceConverter
{
    public class nIceConverter
    {
        private TextWriter output;

        public nIceConverter(TextWriter output)
        {
            this.output = output;
        }

        public bool Convert(string inFile, string outFile)
        {
            Mesh model = null;

            if (inFile.Substring(inFile.Length-4) == ".obj")
            {
                model = FromObj.LoadMesh(inFile, output);
            }

            if (model == null)
            {
                output.WriteLine("Load failed.");
                return false;
            }
            output.WriteLine("Load succeeded.");

            if (!WriteNcf(model, outFile))
            {
                output.WriteLine("Write failed.");
                return false;
            }
            output.WriteLine("Write succeeded.");

            return true;
        }

        private bool WriteNcf(Mesh model, string outFile)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(outFile, FileMode.Create)))
            {
                ASCIIEncoding asen = new ASCIIEncoding();

                writer.Write(asen.GetBytes("nIceGame"));    //content type
                writer.Write((UInt16)0);                    //file version
                writer.Write(asen.GetBytes("MDL"));         //content type
                writer.Write((UInt16)0);                    //content version

                writer.Write((Int32)model.Vertices.Count);
                for (int i = 0; i < model.Vertices.Count; i++)
                {
                    writer.Write(model.Vertices[i].Position.X);
                    writer.Write(model.Vertices[i].Position.Y);
                    writer.Write(model.Vertices[i].Position.Z);

                    writer.Write(model.Vertices[i].Normal.X);
                    writer.Write(model.Vertices[i].Normal.Y);
                    writer.Write(model.Vertices[i].Normal.Z);

                    writer.Write(model.Vertices[i].TexCoord.X);
                    writer.Write(model.Vertices[i].TexCoord.Y);
                }

                writer.Write((Int32)model.Indices.Count);
                for (int i = 0; i < model.Indices.Count; i++)
                    writer.Write(model.Indices[i]);
            }

            return true;
        }
    }
}